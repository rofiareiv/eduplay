<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->id();
            $table->string("video_url")->nullable();
            $table->string("video_title")->nullable();
            $table->string("video_slug")->nullable();
            $table->integer('click_count')->nullable();
            $table->unsignedBigInteger("created_by")->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign("created_by")->references("id")->on("users")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
