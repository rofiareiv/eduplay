@extends('index')

@push('css')
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
@endpush

@section('content')
<!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Studio</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Video Add URL</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Video <small>Add URL</small></h3>
						</div>
            <form action="" method="post" id="createVideo">
							<div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="title">Title</label>
                      <input type="text" name="title" class="form-control" id="title" placeholder="Enter title">
                    </div>
                    <div class="form-group">
                      <label for="url">URL</label>
                      <input type="text" name="url" class="form-control" id="url" placeholder="Enter link url, exp: https://youtu.be/wPdzGrpgGbo">
                    </div>
                    <div class="form-group">
                      <label for="category">Category</label>
                      <select name="category" id="category" data-placeholder="Select a Category" class="form-control select2" style="width: 100%;">
                        <option selected="selected">Alabama</option>
                        <option>Alaska</option>
                        <option>California</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="playlist">Playlist</label>
                      <select name="playlist" id="playlist" data-placeholder="Select a Playlist" class="form-control select2" multiple="multiple" style="width: 100%;">
                        <option>Alabama</option>
                        <option>Alaska</option>
                        <option>California</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="tag">Tags</label>
                      <select name="tag" id="tag" data-placeholder="Select a Tag" class="form-control select2" multiple="multiple" style="width: 100%;">
                        <option>Alabama</option>
                        <option>Alaska</option>
                        <option>California</option>
                      </select>
                    </div>
                  </div>
                </div>
							</div>
              <div class="card-footer">
                <button type="button" class="btn btn-warning">Cancel</button>
                <button type="submit" class="btn btn-primary float-right"> Save </button>
              </div>
            </form>
					</div>
				</div>
			</div>
		</div>
</section>
@endsection

@push('scripts')
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    $('.select2').select2();

    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  });
</script>
@endpush
