@extends('index')

@section('content')
  <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">All Your Videos</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Your Videos</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="card card-success">
                <div class="card-body">
                    <div class="row">
                        <a href="/">
                            <div class="col-md-12 col-lg-6 col-xl-3">
                                <div class="card mb-2 bg-gradient-dark">
                                    <img class="card-img-top" src="../dist/img/photo1.png" alt="Dist Photo 1">
                                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                                        <h5 class="card-title text-primary text-white">Judul</h5>
                                        <p class="card-text text-white pb-2 pt-1">Nama Channel</p>
                                        <a href="#" class="text-white">Tanggal Update</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2 bg-gradient-dark">
                            <img class="card-img-top" src="../dist/img/photo1.png" alt="Dist Photo 1">
                            <div class="card-img-overlay d-flex flex-column justify-content-end">
                                <h5 class="card-title text-primary text-white">Card Title</h5>
                                <p class="card-text text-white pb-2 pt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.</p>
                                <a href="#" class="text-white">Last update 2 mins ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection

@push('script')

@endpush
