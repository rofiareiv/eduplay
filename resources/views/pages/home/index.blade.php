@extends('index')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>New Videos</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>53<sup style="font-size: 20px">%</sup></h3>

                <p>New Playlist</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>44</h3>

                <p>User Registrations</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="card card-success">
                <div class="card-body">
                    <div class="row">
                        <a href="/">
                            <div class="col-md-12 col-lg-6 col-xl-3">
                                <div class="card mb-2 bg-gradient-dark">
                                    <img class="card-img-top" src="../dist/img/photo1.png" alt="Dist Photo 1">
                                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                                        <h5 class="card-title text-primary text-white">Judul</h5>
                                        <p class="card-text text-white pb-2 pt-1">Nama Channel</p>
                                        <a href="#" class="text-white">Tanggal Update</a>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2 bg-gradient-dark">
                            <img class="card-img-top" src="../dist/img/photo1.png" alt="Dist Photo 1">
                            <div class="card-img-overlay d-flex flex-column justify-content-end">
                                <h5 class="card-title text-primary text-white">Card Title</h5>
                                <p class="card-text text-white pb-2 pt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.</p>
                                <a href="#" class="text-white">Last update 2 mins ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo2.png" alt="Dist Photo 2">
                            <div class="card-img-overlay d-flex flex-column justify-content-center">
                                <h5 class="card-title text-white mt-5 pt-2">Card Title</h5>
                                <p class="card-text pb-2 pt-1 text-white">
                                Lorem ipsum dolor sit amet, <br>
                                consectetur adipisicing elit <br>
                                sed do eiusmod tempor.
                                </p>
                                <a href="#" class="text-white">Last update 15 hours ago</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-3">
                            <div class="card mb-2">
                            <img class="card-img-top" src="../dist/img/photo3.jpg" alt="Dist Photo 3">
                            <div class="card-img-overlay">
                                <h5 class="card-title text-primary">Card Title</h5>
                                <p class="card-text pb-1 pt-1 text-white">
                                Lorem ipsum dolor <br>
                                sit amet, consectetur <br>
                                adipisicing elit sed <br>
                                do eiusmod tempor. </p>
                                <a href="#" class="text-primary">Last update 3 days ago</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection
