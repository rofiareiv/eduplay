## About Eduplay

Eduplay is a web application for education video playlist with elegant UI, user friendly, and anywhere access. Build with Laravel,
modern php framework.

## How to clone

If you want to clone this repo, do command above

```
git clone https://gitlab.com/rofiareiv/eduplay.git
cd eduplay
composer install
php artisan migrate
php artisan laravolt:indonesia:seed
php artisan db:seed
```
## After Change

And after change the project, do this command
```
git branch new_branch
git checkout new_branch
git addd .
git commit -m "your comment or note"
git push -u origin new_branch
```

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling.

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

The EduPlay is licensed under the PRODISTIK education and [barengsaya.com](https://barengsaya.com).

## Contact

- [www.barengsaya.com](https://barengsaya.com)
- Telegram: [BarengSaya.Com](https://t.me/BarengSayaCom)

## Donate

- Jenius: @rofiareiv
- PayPal: paypal.me/rofiareiv
- GoPay | Dana | Ovo: 082334122336
