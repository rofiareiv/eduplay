<?php

namespace App\Http\Controllers;

use App\Models\TagVideo;
use Illuminate\Http\Request;

class TagVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TagVideo  $tagVideo
     * @return \Illuminate\Http\Response
     */
    public function show(TagVideo $tagVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TagVideo  $tagVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(TagVideo $tagVideo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TagVideo  $tagVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TagVideo $tagVideo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TagVideo  $tagVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(TagVideo $tagVideo)
    {
        //
    }
}
