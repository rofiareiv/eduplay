<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;

    protected $fillable = [
      'video_url',
      'video_title',
      'video_slug',
      'click_count',
    ];

    protected $hidden = ['created_by'];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
